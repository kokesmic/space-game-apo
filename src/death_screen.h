/*
 * File name: death_screen.h
 * Date:      2022/05/17 22:57
 * Author:    Pavel Klas
 * License:   MIT
 *
 * Description:
 * Draws death message and victory message for both players and lets them see who won.
 */

#ifndef __DEATH_SCREEN_H__
#define __DEATH_SCREEN_H__

// - includes -----------------------------------------------------------------
#include "renderer.h"

// Resets rand() seed
void init_random();

// Returns a random message string from the victory or defeat array
char *get_random_message(bool win);

/**
 * @brief Draws the game over screen, announces which player won and displays the victory/defeat messages.
 *  
 * @param winner Id of winning player.
 * @param lcd_mem_base Memory pointer to the LCD.
 * @param membase Memory pointer to the board.
 * @param fb Framebuffer for the screen
 * @param width framebuffer width
 * @param height framebuffer height
 */
void game_over(int winner, unsigned char *lcd_mem_base, unsigned char *mem_base, framebuffer_t *fb, int width, int height);

#endif
