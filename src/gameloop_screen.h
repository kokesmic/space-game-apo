/*
 * File name: gameloop_screen.h
 * Date:      2022/05/18 21:59
 * Author:    Michal Kokeš
 * License:   MIT
 *
 * Description:
 * Contains game drawing method and game update method all neatly packed into game loop with a timer.
 */

#ifndef __GAMELOOP_SCREEN_H__
#define __GAMELOOP_SCREEN_H__

#include <stdbool.h>
#include "renderer.h"


/**
 * @brief Renderes all entities and background.
 * 
 */
//void render_gameloop()

//void update_gameloop()

/**
 * @brief Updates the game loop each loop_delay milliseconds.
 * 
 * @param winner *int Pointer to the winner of the game.
 * @param coop bool If true the game is coop, and the other player is controlled by the AI.
 * @param lcd_mem_base unsigned char * Pointer to the beginning of the LCD memory.
 * @param mem_base unsigned char * Pointer to the beginning of the memory.
 * @param loop_delay unsigned int The delay between each game loop.
 * @param fb *FrameBuffer Pointer to the frame buffer.
 * @param screen_width unsigned int The width of the screen.
 * @param screen_height unsigned int The height of the screen.
 */
void game_loop(int *winner, bool coop, unsigned char *lcd_mem_base, unsigned char *mem_base, struct timespec loop_delay, framebuffer_t *fb, int screen_width, int screen_height);

#endif
/* end of gameloop_screen.h */
