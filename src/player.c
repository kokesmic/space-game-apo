/*
 * File name: player.c
 * Date:      2022/05/08 14:15
 * Author:    Michal Kokeš
 * License:   MIT
 *
 * Description:
 * Player struct containing player data and functions for manipulating it.
 */

// - defines -------------------------------------------------------------------
#define PLAYER_PREDICTION_FRAMES 30
#define PLAYER_BULLET_AI_RADIUS 150
#define PLAYER_COOLDOWN_FRAMES 20
#define AI_DIFFICULTY_LEVEL 0.2

// - includes -----------------------------------------------------------------
#include <stdbool.h>
#include <math.h>
#include "player.h"
#include "vector.h"

// - function ----------------------------------------------------------------
player_t *player_init(player_t *player, int id, bool ai, color_t color, vector2_t position, vector2_t velocity)
{
    player->ai = ai;
    player->position = position;
    player->id = id;
    player->color = color;
    player->velocity = velocity;
    player->health = PLAYER_DEFAULT_HEALTH;
    player->cooldown = 0;
    player->button_time = 0;
    return player;
}

// - function ----------------------------------------------------------------
void player_update(player_t *player, bool *shoot, bool button, vector2_t direction, int screen_width, int screen_height)
{
    bool attempt_to_shoot = *shoot;
    // decrease the velocity by a factor of 0.1
    vector2_multiply(&player->velocity, PLAYER_SLOW_DOWN);

    if (button)
    {
        // check if player button was pressed or is being held down
        if (player->button_time > BUTTON_PRESS_TRESHOLD)
        {
            // add direction to velocity and clamp betweeen 0 and MAX_VELOCITY
            vector2_t dir = direction;
            // multiply by speed
            double speed = player->ai ? AI_DIFFICULTY_LEVEL : PLAYER_DEFAULT_SPEED;
            vector2_multiply(&dir, speed);
            // add to velocity
            vector2_add(&player->velocity, dir);
        }
        else
        {
            // increase the time the button was pressed
            player->button_time += 0.1;
        }
    }
    else if (player->button_time != 0)
    {
        // if button is under treshold act as if it was pressed
        if ((player->button_time < BUTTON_PRESS_TRESHOLD) && player->cooldown == 0)
        {
            attempt_to_shoot = true;
        }
        player->button_time = 0;
    }

    // check if player is attempting to shoot and if cooldown is over
    if (attempt_to_shoot && player->cooldown <= 0)
    {
        // set cooldown to PLAYER_COOLDOWN_FRAMES
        player->cooldown = PLAYER_COOLDOWN_FRAMES;
        // set shoot to false
        *shoot = true;
    }
    else
    {
        *shoot = false;
    }

    // clamp to max velocity
    vector2_clamp(&player->velocity, -PLAYER_VELOCITY_LIMIT, PLAYER_VELOCITY_LIMIT);
    // update position
    vector2_add(&player->position, player->velocity);
    // modulo position
    vector2_modulo(&player->position, screen_width, screen_height);

    // update cooldown
    if (player->cooldown > 0)
        player->cooldown--;
}

// - function ----------------------------------------------------------------
uint8_t player_scale_health(int health, bool left)
{

    // if left bitshift number to the left
    if (left)
        return 0xFF << (8 - (int)(health / 12.5));
    // if right bitshift number to the right
    else
        return 0xFF >> (8 - (int)(health / 12.5));
}

// - function ----------------------------------------------------------------
uint8_t player_scale_cooldown(int cooldown, bool left)
{
    cooldown *= 5;
    // if left bitshift number to the left
    if (left)
        return 0xFF << (8 - (int)(cooldown / 12.5));
    // if right bitshift number to the right
    else
        return 0xFF >> (8 - (int)(cooldown / 12.5));
}

// - function ----------------------------------------------------------------
void get_ai_input(player_t *player_ai, player_t *player_hp, vector2_t *direction, bool *shoot)
{
    // slowly change direction towards player
    vector2_t future_position_player = player_hp->velocity;
    vector2_multiply(&future_position_player, 5);
    vector2_add(&future_position_player, player_hp->position);
    vector2_sub(&future_position_player, player_ai->position);
    vector2_normalize(&future_position_player);

    // shoot if player is close
    double distance = vector2_distance(player_ai->position, player_hp->position);
    if (distance < PLAYER_BULLET_AI_RADIUS)
    {
        *shoot = true;
    }

    direction->x = future_position_player.x;
    direction->y = future_position_player.y;
}
