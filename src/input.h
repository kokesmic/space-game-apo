/*
 * File name: input.h
 * Date:      2022/05/15 22:17
 * Author:    Pavel Klas
 * License:   MIT
 * 
 * Description:
 * Contains all the methods to interact with the board and the input on it.
 */

#ifndef __INPUT_H__
#define __INPUT_H__

// - includes -----------------------------------------------------------------
#include <stdbool.h>
#include <stdio.h>
#include "renderer.h"

/** 
 * @brief Get the knob rotation value.
 * 
 * @param knob_id The knob to get the rotation value from.
 * @param mem_base The base address of the memory where to look for the knob.
 * @return double The rotation value of the knob. 0 - 360 degrees.
 */
double get_knob_rotation_value(int knob_id, unsigned char *mem_base);

/**
 * @brief Get the knob button value from the board.
 * 
 * @param knob_id The knob button to get the value from.
 * @param mem_base The base address of the memory where to look for the knob.
 * @return true if the button is pressed. 
 */
bool get_knob_button_value(int knob_id, unsigned char *mem_base);

/**
 * @brief Set the led value on the board.
 * 
 * @param led_id The led to set the value on. (0 = left, 1 = right)
 * @param mem_base The base address of the memory where to look for the led.
 * @param color The color of the led.
 */
void set_led_value(int led_id, unsigned char *mem_base, color_t color);

/**
 * @brief Set the ledstrips on the board to the bool array leds.
 * 
 * @param led_arr_id The ledstrips to set the value on. (0 = left most, 1 = left center, 2 = right center, 3 = right most)
 * @param value The value to set the led to. (0 - 100) 
 * @param mem_base The base address of the memory where to look for the leds.
 */
void set_ledstrip_value(uint8_t value[4], unsigned char *mem_base);

#endif
/* end of input.h */
