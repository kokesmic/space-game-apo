/*
 * File name: input.h
 * Date:      2022/05/15 10:42
 * Author:    Pavel Klas
 * License:   MIT
 *
 * Description:
 * Input interface for the board.
 */

// - includes -----------------------------------------------------------------
#include "input.h"
#include "renderer.h"
#include "mzapo_regs.h"
#include <stdbool.h>
#include <stdio.h>
#include <unistd.h>

// - defines ------------------------------------------------------------------
#define BYTE_TO_DEGREE_RATIO 360.0 / 255.0
#define BUTTON_PRESS SPILED_REG_KNOBS_8BIT_o + 3

// - function ----------------------------------------------------------------
bool get_knob_button_value(int knob_id, unsigned char *mem_base)
{
    return mem_base[BUTTON_PRESS] & (1 << (2 - knob_id));
}

// - function ----------------------------------------------------------------
double get_knob_rotation_value(int knob_id, unsigned char *mem_base)
{
    unsigned char *knob_base = mem_base + SPILED_REG_KNOBS_8BIT_o + knob_id;

    return (double)*knob_base * BYTE_TO_DEGREE_RATIO;
}

// - function ----------------------------------------------------------------
void set_led_value(int led_id, unsigned char *mem_base, color_t color)
{
    if (led_id == 0)
    {
        *(volatile uint32_t *)(mem_base + SPILED_REG_LED_RGB1_o) = color.b | (color.g << 8) | (color.r << 16);
    }
    else
    {
        *(volatile uint32_t *)(mem_base + SPILED_REG_LED_RGB2_o) = color.b | (color.g << 8) | (color.r << 16);
    }
}

// - function ----------------------------------------------------------------
void set_ledstrip_value(uint8_t value[4], unsigned char *mem_base)
{
    // clear the leds
    *(uint32_t *)(mem_base + SPILED_REG_LED_LINE_o) = 0;
    // Set the leds
    *(uint32_t *)(mem_base + SPILED_REG_LED_LINE_o) = value[0] << 24 | (value[1] << 16) | (value[2] << 8) | (value[3] << 0);
}
