/*
 * File name: death_screen.c
 * Date:      2022/05/19 02:58
 * Author:    Pavel Klas
 * License:   MIT
 * 
 * Description:
 * Draws death message and victory message for both players and lets them see who won.
 */

// - defines -------------------------------------------------------------------
#define TEXTURE_END_SCREEN "/tmp/space-game/textures/game_over_screen.ppm"
#define DEFEAT_LIST_LEN 32
#define VICTORY_LIST_LEN 17

// - includes ------------------------------------------------------------------
#include <math.h>
#include <time.h>
#include <stdbool.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include "death_screen.h"
#include "renderer.h"
#include "input.h"


// - function ------------------------------------------------------------------
void init_random()
{
    srand(time(NULL));
}

// - function ------------------------------------------------------------------
char *get_random_message(bool win)
{
    char *victory_list[VICTORY_LIST_LEN] =
        {"ok I guess",
         "you win, cool",
         "you did it, yay",
         "Sheeeeesh",
         "Acceptable outcome",
         "Don't get used to it",
         "Probably a fluke",
         "Lucky play",
         "You probably cheated anyway",
         "Rise to fall another day",
         "wooow, so exciting",
         "Savor this victory",
         "Look at you, finally winning for once",
         "What a perfectly decent play!",
         "Alright",
         "nice nice nice nice nice nice nice nice nice nice nice nice nice nice",
         "Aww, goood job sweetieee"};

    char *defeat_list[DEFEAT_LIST_LEN] =
        {"YOU DIED",
         "Game Over",
         "Yikes, must have hurt",
         "How do you lose that?",
         "Maybe next time",
         "Having a bad day, huh",
         "Never played a game before?",
         "Are you even trying",
         "Wait, I missed it, you lost??",
         "Guess you're not a Real Gamer",
         "Dont't worry, you'll get better. maybe.",
         "Pro tip: Don't die in dumb ways",
         "Pro tip: Try to win maybe",
         "Yeah, if I were you I'd stop trying too",
         "lol gottem", "Pro tip: Getting hit bad",
         "Oh, were you not watching the screen?",
         "you lose. again. duh.",
         "People make mistakes. You're pushing it though.",
         "Did you know: Bullets should be avoided",
         "You're almost on the top 100 leaderboard!",
         "hahaha", "Well, it's not like I expected you to win",
         "Good part is your loss surprised nobody",
         "A dozen more hours and you'll almost be average!",
         "You know you should learn from your mistakes, right",
         "Some people learn from mistakes. Try that?",
         "Not your strong side huh",
         "I hope your brains are better than your hands",
         "I bet you look like you were drawn with my left hand",
         "ggez",
         "For some reason I believed in you this time, my bad"};

    if (win)
    {
        return victory_list[rand() % VICTORY_LIST_LEN];
    }
    else
    {
        return defeat_list[rand() % DEFEAT_LIST_LEN];
    }
}

// - function ------------------------------------------------------------------
void game_over(int winner, unsigned char *lcd_mem_base, unsigned char *mem_base, framebuffer_t *fb, int width, int height)
{
    // clear screen by memcopy image from background
    int w, h, c;
    unsigned char *img = load_ppm_image(TEXTURE_END_SCREEN, &w, &h, &c);
    framebuffer_t background = {
        .width = w,
        .height = h,
        .pixels = (unsigned short *)malloc(h * w * 2)};
    ppm_to_framebuffer(img, &background);
    free(img);

    copy_frame_buffer(&background, fb);
    draw_screen(fb, lcd_mem_base);

    // load font
    font_descriptor_t *fdes = &font_winFreeSystem14x16;

    // draw menu
    clear_screen(fb);

    // define colors
    color_t red_color = {200, 100, 50};
    color_t white_color = {255, 255, 255};
    color_t black_grey = {4, 4, 4};
    color_t serious_black = {0, 0, 0};

    // update leds
    set_led_value(0, mem_base, serious_black);
    set_led_value(1, mem_base, serious_black);

    int time = 0;
    int offset = 50;
    int width_half = width / 2;
    int height_half = height / 2;
    int center = height_half - fdes->height / 2;
    bool clr = false;
    char *death_message = get_random_message(winner == 1);
    char *death_message2 = get_random_message(winner == 2);

    while (true)
    {
        copy_frame_buffer(&background, fb);
        // draw winner
        if (time % 25 == 0)
            clr = !clr;
        color_t press_btn_clr = clr == 0 ? red_color : serious_black;
        if (winner == 1)
        {
            draw_text_centered(width_half, center - 25, "PLAYER 2 LOST", black_grey, fdes, fb);
        }
        else
        {
            draw_text_centered(width_half, center - 25, "PLAYER 1 LOST", black_grey, fdes, fb);
        }

        // create rainbow color from time
        color_t rnb = {(sin(time/17) + 1) * 20,(sin(time/23) + 1) * 20, (sin(time/13) + 1) * 20};
        set_led_value(0, mem_base, rnb);
        set_led_value(1, mem_base, rnb);
    
        int pos = width - 7.5 * strlen(death_message2) - 35;
        int offset1 = pos + (offset * 2) > width ? width -1:  pos + (offset * 2);
        draw_text(30 - (offset * 2), center + 110, death_message, white_color, fdes, fb);
        draw_text(offset1, center - 80, death_message2, white_color, fdes, fb);
        draw_text_centered(width_half, center + 25, "Press any button to return to the menu screen", press_btn_clr, fdes, fb);
        draw_screen(fb, lcd_mem_base);

        if (get_knob_button_value(0, mem_base) || get_knob_button_value(1, mem_base) || get_knob_button_value(2, mem_base))
        {
            printf("Returning to menu\n");
            break;
        }

        if (time++ < 46)
        {
            offset /= 1.05;
        }
    }
    clear_screen(fb);
    draw_screen(fb, lcd_mem_base);
    sleep(2);
}
