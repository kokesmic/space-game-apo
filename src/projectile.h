/*
 * File name: projectile.h
 * Date:      2022/05/08 14:13
 * Author:    Michal Kokeš
 * License:   MIT
 *
 * Description:
 * Projectile struct containing projectile data and functions for manipulating it.
 */

#ifndef __PROJECTILE_H__
#define __PROJECTILE_H__

#include <stdbool.h>
#include "renderer.h"
#include "player.h"
#include "vector.h"

#define MAX_BULLET_COUNT 8
#define BULLET_LIFE_TIME 100
#define PLAYER_BULLET_COLLISION_RADIUS 15
#define PLAYER_BULLET_DAMAGE 10
#define BULLET_HIT_VELOCITY_MULTIPLIER 0.5
#define BULLET_HIT_RADIUS 10

/**
 * @brief Player struct containing projectile data.
 */
typedef struct Projectile
{
    int player_id;
    vector2_t position;
    vector2_t velocity;
    double lifetime;
    bool active;
} bullet_t, projectile_t;

/**
 * @brief Mallocs a bullet and copies data from the given parameters.
 *
 * @param position Position of the bullet.
 * @param velocity Velocity of the bullet.
 *
 * @return Pointer to the bullet.
 */
bullet_t *bullet_init(vector2_t position, vector2_t velocity);

/**
 * @brief Destroys the bullet
 *
 * @param bullet Pointer to the bullet.
 */
void bullet_destroy(bullet_t *bullet);

/**
 * @brief Updates the bullets positions by adding velocity.
 *
 * @param bullet_count Number of bullets.
 * @param bullets array of bullets.
 * @param screen_width screen width.
 * @param screen_height screen height.
 */
void bullet_update(int bullet_count, bullet_t bullet[], int screen_width, int screen_height);

/**
 * @brief Adds a bullet into a bullet list
 *
 * @param bullets array of bullets.
 * @param pos position of the bullet.
 * @param dir direction of the bullet.
 */
void buller_add(bullet_t bullets[], vector2_t pos, vector2_t dir);

/**
 * @brief Reorders the bullets array by removing inactive bullets.
 *
 * @param bullet_count count of bullets.
 * @param bullets array of bullets.
 */
void bullet_reorder(int *bullet_count, bullet_t bullets[]);

/**
 * @brief Checks if the bullet collides with player.
 *
 * @param player  Player to check collision with.
 * @param bullets Array of bullets to check collision with.
 * @param blink_leds Bool pointer to the blink leds flag.
 * @return true if the bullet collides with player, false otherwise.
 */
bool player_bullet_collision(player_t *player, bullet_t *bullets, bool *blink_leds);

#endif
/* end of projectile.h */