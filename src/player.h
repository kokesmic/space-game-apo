/*
 * File name: player.h
 * Date:      2022/05/08 13:30
 * Author:    Michal Kokeš
 */

#ifndef __PLAYER_H__
#define __PLAYER_H__

// - includes -----------------------------------------------------------------
#include "vector.h"
#include "renderer.h"

// - defines ------------------------------------------------------------------
#define PLAYER_DEFAULT_HEALTH 100
#define PLAYER_DEFAULT_SPEED 1.1
#define PLAYER_COOLDOWN_TIME 3
#define PLAYER_SLOW_DOWN 0.98
#define PLAYER_VELOCITY_LIMIT 5
#define BUTTON_PRESS_TRESHOLD 0.3

/**
 * @brief Player struct containing player data.
 */
typedef struct Player
{
    bool ai;
    int id;
    color_t color;
    int health;
    vector2_t position;
    vector2_t velocity;
    double cooldown;
    double button_time;
} player_t;

/**
 * @brief Initializes the player with default values.
 *
 * @param player Player to initialize.
 * @param id Player id.
 * @param ai Whether the player is controlled by AI.
 * @param color Color of the player.
 * @param position Position of the player.
 * @param velocity Velocity of the player.
 * @return player_t Player initialized.
 */
player_t *player_init(player_t *player, int id, bool ai, color_t color, vector2_t position, vector2_t velocity);

/**
 * @brief update the player positiona by adding velocity in a direction and decreasing cooldown.
 *
 * @param player Player to update.
 * @param shoot Pointer to shoot variable.
 * @param button Player button pressed.
 * @param direction Direction to add velocity in.
 * @param screen_width Width of the screen.
 * @param screen_height Height of the screen.
 */
void player_update(player_t *player, bool *shoot, bool button, vector2_t direction, int screen_width, int screen_height);

/**
 * @brief Scale health of the player to appear as a binary line.
 *
 * @param health Health of the player.
 * @param left Whether the cooldown is on the left or right of the screen.
 * @return uint8_t Scaled health to form a binary line.
 */
uint8_t player_scale_health(int health, bool left);

/**
 * @brief Scale cooldown of the player to appear as a binary line.
 * 
 * @param cooldown Cooldown of the player.
 * @param left Whether the cooldown is on the left or right of the screen.
 * @return uint8_t Scaled cooldown to form a binary line.
 */
uint8_t player_scale_cooldown(int cooldown, bool left);

/**
 * @brief Draws the player on the screen.
 *
 * @param player_ai Player* to control by the AI.
 * @param player_hp Player* to target.
 * @param direction Output direction of the player.
 * @param renderer Renderer to draw on.
 * @param shoot Pointer to shoot variable.
 */
void get_ai_input(player_t *player_ai, player_t *player_hp, vector2_t *direction, bool *shoot);
#endif

/* end of player.h */
