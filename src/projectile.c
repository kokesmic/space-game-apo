/*
 * File name: projectile.c
 * Date:      2022/05/08 14:15
 * Author:    Michal Kokeš
 */

// - includes -----------------------------------------------------------------
#include "projectile.h"
#include "player.h"
#include "input.h"

// - function ----------------------------------------------------------------
void bullet_update(int bullet_count, bullet_t bullet[], int screen_width, int screen_height)
{
    // move bullets
    for (int i = 0; i < bullet_count; i++)
    {
        vector2_add(&bullet[i].position, bullet[i].velocity);
        if (bullet[i].lifetime-- <= 0)
        {
            bullet[i].active = false;
        }
        // check if bullet is outside of screen
        if (bullet[i].lifetime-- <= 0 || bullet[i].position.x < 0 || bullet[i].position.x > screen_width || bullet[i].position.y < 0 || bullet[i].position.y > screen_height)
        {
            bullet[i].active = false;
        }
    }
}

// - function ----------------------------------------------------------------
void bullet_reorder(int *bullet_count, bullet_t bullets[])
{
    // reorder bullets
    for (int i = 0; i < *bullet_count; i++)
    {
        if (!bullets[i].active)
        {
            // move all active bullets to the end of the array
            for (int j = i; j < *bullet_count - 1; j++)
            {
                bullets[j] = bullets[j + 1];
            }
            // decrease bullet count
            (*bullet_count)--;
        }
    }
}

// - function ----------------------------------------------------------------
bool player_bullet_collision(player_t *player, bullet_t *bullet, bool *blink_leds)
{
    if(bullet->active && vector2_distance(player->position, bullet->position) < PLAYER_BULLET_COLLISION_RADIUS)
    {   
        // damage player
        player->health -= PLAYER_BULLET_DAMAGE;
        *blink_leds = true;
        // add player velocity in opposite direction of bullet
        vector2_multiply(&bullet->velocity, BULLET_HIT_VELOCITY_MULTIPLIER);
        vector2_add(&player->velocity, bullet->velocity);

        bullet->active = false;
        printf("Player %d hit by bullet. Health: %d\n", player->id, player->health);
        return true;
    }
    return false;
}
