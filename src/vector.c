/*
 * File name: vector.c
 * Date:      2022/05/08 12:41
 * Author:    Michal Kokeš
 * License:   MIT
 * 
 * Description:
 * Vector struct containing vector data and functions for manipulating it.
 */

// - includes -----------------------------------------------------------------
#include "vector.h"
#include <math.h>

// - function ----------------------------------------------------------------
vector2_t *vector2_add(vector2_t *v1, vector2_t v2)
{
    v1->x += v2.x;
    v1->y += v2.y;
    return v1;
}

// - function ----------------------------------------------------------------
vector2_t *vector2_sub(vector2_t *v1, vector2_t v2)
{
    v1->x -= v2.x;
    v1->y -= v2.y;
    return v1;
}

// - function ----------------------------------------------------------------
vector2_t *vector2_multiply(vector2_t *v1, double n)
{
    v1->x *= n;
    v1->y *= n;
    return v1;
}

// - function ----------------------------------------------------------------
float vector2_distance(vector2_t v1, vector2_t v2)
{
    return sqrt(pow(v1.x - v2.x, 2) + pow(v1.y - v2.y, 2));
}

// - function ----------------------------------------------------------------
void vector2_from_rotation(vector2_t *v1, double rotation)
{
    v1->x = cos(rotation * M_PI / 180);
    v1->y = sin(rotation * M_PI / 180);
}

// - function ----------------------------------------------------------------
void vector2_clamp(vector2_t *v1, double min, double max)
{
    if (v1->x < min)
        v1->x = min;
    if (v1->x > max)
        v1->x = max;
    if (v1->y < min)
        v1->y = min;
    if (v1->y > max)
        v1->y = max;
}

// - function ----------------------------------------------------------------
void vector2_modulo(vector2_t *v1, double mod_x, double mod_y)
{
    v1->x = fmod(v1->x, mod_x);
    v1->y = fmod(v1->y, mod_y);
    v1->x = v1->x < 0 ? v1->x + mod_x : v1->x;
    v1->y = v1->y < 0 ? v1->y + mod_y : v1->y;
}

// - function ----------------------------------------------------------------
void vector2_normalize(vector2_t *v1)
{
    double length = sqrt(pow(v1->x, 2) + pow(v1->y, 2));
    v1->x /= length;
    v1->y /= length;
}

// - function ----------------------------------------------------------------
void vector2_normal(vector2_t *v1, vector2_t v2)
{
    double length = sqrt(pow(v1->x - v2.x, 2) + pow(v1->y - v2.y, 2));
    v1->x = (v1->x - v2.x) / length;
    v1->y = (v1->y - v2.y) / length;
}