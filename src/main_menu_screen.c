/*
 * File name: main_menu_screen.c
 * Date:      2022/05/19 02:52
 * Author:    Michal Kokeš
 * License:   MIT
 *
 * Description:
 * Draws main menu and lets player choose game mode or exit
 */

// - defines -------------------------------------------------------------------
#define TEXTURE_MAIN_MENU "/tmp/space-game/textures/main_menu.ppm"

// - includes ------------------------------------------------------------------
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include "main_menu_screen.h"
#include "font_types.h"
#include "renderer.h"
#include "input.h"

// - function ------------------------------------------------------------------
void main_menu(int *menu_selection, struct timespec loop_delay, unsigned char *lcd_mem_base, unsigned char *mem_base, framebuffer_t *fb)
{
    // load font
    font_descriptor_t *fdes = &font_winFreeSystem14x16;

    // load background ppm image and convert to framebuffer
    int w, h, c;
    unsigned char *img = load_ppm_image(TEXTURE_MAIN_MENU, &w, &h, &c);
    framebuffer_t background = {.width = w, .height = h,.pixels = (unsigned short *)malloc(h * w * 2)};
    ppm_to_framebuffer(img, &background);
    free(img);

    // init colors that will be used for drawing menu
    color_t white        = RGB_COLOR_WHITE;
    color_t black        = RGB_COLOR_BLACK;
    color_t dark_gray    = RGB_COLOR_DARK_GRAY;
    color_t red          = RGB_COLOR_RED;
    color_t green        = RGB_COLOR_GREEN;
    color_t blue         = RGB_COLOR_BLUE;

    // clear leds
    set_led_value( 0, mem_base, black);
    set_led_value( 1, mem_base, black);
    uint8_t black_arr[4] = {0,0,0,0};
    set_ledstrip_value( black_arr, mem_base);

    // copy background to framebuffer
    copy_frame_buffer(&background, fb);
    while (true)
    {
        draw_text(25, 110, "MAIN MENU", white, fdes, fb);

        // draw circle with border and text for each option
        draw_text(40, 140, "1. PLAY - single", red, fdes, fb);
        draw_circle_outline(25, 145, 5, 2, red, white, fb);
        draw_text(40, 170, "2. PLAY - coop", green, fdes, fb);
        draw_circle_outline(25, 175, 5, 2, green, white, fb);
        draw_text(40, 200, "3. EXIT", blue, fdes, fb);
        draw_circle_outline(25, 205, 5, 2, blue, white, fb);

        // print hint and version in the bottom center
        draw_text(fb->width / 2 - 150, fb->height - fdes->height * 2, "PRESS the corresponding colour to continue", white, fdes, fb);
        draw_text(fb->width / 2 - 25, fb->height - fdes->height, VERSION, dark_gray, fdes, fb);
        draw_screen(fb, lcd_mem_base);

        // get input from user and update menu selection
        if (get_knob_button_value(0, mem_base))
        {
            *menu_selection = SINGLEPLAYER;
            break;
        }
        else if (get_knob_button_value(1, mem_base))
        {
            *menu_selection = COOP;
            break;
        }
        else if (get_knob_button_value(2, mem_base))
        {
            *menu_selection = EXIT;
            break;
        }

        // wait for loop_delay
        clock_nanosleep(CLOCK_MONOTONIC, 0, &loop_delay, NULL);
    }
    // wait 1 second to prevent accidental missfire
    sleep(1);
}