/*
 * File name: main_menu_screen.h
 * Date:      2022/05/19 02:30
 * Author:    Michal Kokeš
 * License:   MIT
 *
 * Description:
 * Draws main menu and lets player choose game mode or exit
 */

#ifndef __MAIN_MENU_SCREEN_H__
#define __MAIN_MENU_SCREEN_H__

// - includes -----------------------------------------------------------------
#include "renderer.h"

// - defines -------------------------------------------------------------------
#define VERSION "relese - 1.8"
#define SINGLEPLAYER 1
#define COOP 2
#define EXIT 3

/**
 * @brief draws main menu and lets player choose game mode or exit.
 * 
 * @param menu_selection Pointer to the variable that will contain the selected menu item.
 * @param loop_delay Delay between checks of the button.
 * @param lcd_mem_base Pointer to the base of LCD memory.
 * @param mem_base Pointer to the base of memory.
 * @param fb Pointer to the screen framebuffer.
 */
void main_menu(int *menu_selection, struct timespec loop_delay, unsigned char *lcd_mem_base, unsigned char *mem_base, framebuffer_t *fb);

#endif
/* end of renderer.h */
