/*
 * File name: renderer.c
 * Date:      2022/05/05 16:22
 * Author:    Michal Kokeš
 *
 * Description:
 * All drawing functions and interaction with frame buffer, drawing into the memory buffer and rgb to color conversion.
 *
 * Warning:
 * This file contains some code from Petr Porazil at PiKRON.
 * source: https://cw.fel.cvut.cz/wiki/courses/b35apo/tutorials/11/start
 * all of the code segments are marked with the comment: "Petr Porazil's code" and end with the comment: "Petr Porazil's code end"
 * the code is slightly modified to fit the needs of the project.
 */

// -- INCLUDE FILES ----------------------------------------------------------
#include "renderer.h"
#include "mzapo_parlcd.h"
#include <math.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

// -- FUNCTIONS --------------------------------------------------------------

// - function ----------------------------------------------------------------
void draw_pixel(int x, int y, color_t color, bool loop, framebuffer_t *screen)
{
    // check if x and y are in the screen
    if (loop)
    {
        // x
        x = x % screen->width;
        x = x < 0 ? x + screen->width : x;

        // y
        y = y % screen->height;
        y = y < 0 ? y + screen->height : y;
        screen->pixels[x + screen->width * y] = rgb_to_short(color);
    }
    else if (x >= 0 && x < screen->width && y >= 0 && y < screen->height)
    {
        screen->pixels[x + screen->width * y] = rgb_to_short(color);
    }
}

// - function ----------------------------------------------------------------
void draw_screen(framebuffer_t *screen, unsigned char *parlcd_mem_base)
{
    // write framebuffer to screen
    parlcd_write_cmd(parlcd_mem_base, 0x2c);
    for (int ptr = 0; ptr < screen->width * screen->height; ptr++)
        parlcd_write_data(parlcd_mem_base, screen->pixels[ptr]);
}

// - function ----------------------------------------------------------------
void clear_screen(framebuffer_t *screen)
{
    for (int i = 0; i < screen->width * screen->height; i++)
        screen->pixels[i] = 0;
}

// - function ----------------------------------------------------------------
void copy_frame_buffer(framebuffer_t *src, framebuffer_t *dest)
{
    memcpy(dest->pixels, src->pixels, dest->width * dest->height * sizeof(unsigned short));
}

// - function ----------------------------------------------------------------
unsigned short rgb_to_short(color_t color)
{
    return ((color.r & 0xF8) << 8) | ((color.g & 0xFC) << 3) | ((color.b & 0xF8) >> 3);
}

// - function ----------------------------------------------------------------
color_t short_to_rgb(unsigned short color)
{
    color_t rgb;
    rgb.r = (color >> 8) & 0xF8;
    rgb.g = (color >> 3) & 0xFC;
    rgb.b = (color << 3) & 0xF8;
    return rgb;
}

// - function ----------------------------------------------------------------
void draw_circle(int x, int y, int radius, color_t color, bool loop, framebuffer_t *screen)
{
    for (int u = -radius; u <= radius; u++)
    {
        for (int v = -radius; v <= radius; v++)
        {
            if (u * u + v * v <= radius * radius)
            {
                draw_pixel(x + u, y + v, color, loop, screen);
            }
        }
    }
}

// - function ----------------------------------------------------------------
void draw_circle_outline(int x, int y, int radius, int border, color_t color, color_t outline, framebuffer_t *screen)
{
    int border_radius = radius + border;
    for (int u = -border_radius; u <= border_radius; u++)
    {
        for (int v = -border_radius; v <= border_radius; v++)
        {
            int dist = sqrt(u * u + v * v);
            if (dist <= radius)
            {
                // if distance is smaller than radius, draw pixel
                draw_pixel(x + u, y + v, color, true, screen);
            }
            else if (dist <= border_radius)
            {
                // if distance is smaller than radius + border, draw outline
                draw_pixel(x + u, y + v, outline, true, screen);
            }
        }
    }
}

// - function ----------------------------------------------------------------
void draw_rect(int x, int y, int with, int height, color_t color, framebuffer_t *screen)
{
    // draw rectangle into the screen buffer and modulo over the screen size
    for (int u = 0; u < with; u++)
    {
        for (int v = 0; v < height; v++)
        {
            int x_mod = (u + x) % screen->width;
            int y_mod = (v + y) % screen->height;
            draw_pixel(x_mod, y_mod, color, true, screen);
        }
    }
}

// - helper function ----------------------------------------------------------
// Petr Porazil's code
int char_width(int ch, font_descriptor_t *fdes)
{
    int width;
    if (!fdes->width)
    {
        width = fdes->maxwidth;
    }
    else
    {
        width = fdes->width[ch - fdes->firstchar];
    }
    return width;
}
// Petr Porazil's code end

// - function ----------------------------------------------------------------
// Petr Porazil's code - with small modifications to fit the needs of the project
void draw_char(int x, int y, char ch, font_descriptor_t *fdes, color_t color, framebuffer_t *screen)
{
    int w = char_width(ch, fdes);
    const font_bits_t *ptr;
    if ((ch >= fdes->firstchar) && (ch - fdes->firstchar < fdes->size))
    {
        if (fdes->offset)
        {
            ptr = &fdes->bits[fdes->offset[ch - fdes->firstchar]];
        }
        else
        {
            int bw = (fdes->maxwidth + 15) / 16;
            ptr = &fdes->bits[(ch - fdes->firstchar) * bw * fdes->height];
        }
        int i, j;
        for (i = 0; i < fdes->height; i++)
        {
            font_bits_t val = *ptr;
            for (j = 0; j < w; j++)
            {
                if ((val & 0x8000) != 0)
                {
                    draw_pixel(x + j, y + i, color, false, screen);
                }
                val <<= 1;
            }
            ptr++;
        }
    }
}
// Petr Porazil's code end

// - function ----------------------------------------------------------------
void draw_text(int x, int y, char *text, color_t color, font_descriptor_t *fdes, framebuffer_t *screen)
{
    // sanatize input
    if (x < 0 || x >= screen->width)
        x = x % screen->width;
    if (y < 0 || y >= screen->height)
        y = y % screen->height;

    // draw text into the screen buffer
    for (int i = 0; i < strlen(text); i++)
    {
        draw_char(x, y, text[i], fdes, color, screen);
        x += char_width(text[i], fdes) + 1;
    }
}

// - function ----------------------------------------------------------------
void draw_text_centered(int x, int y, char *text, color_t color, font_descriptor_t *fdes, framebuffer_t *screen)
{
    // sanatize input
    if (x < 0 || x >= screen->width)
        x = x % screen->width;
    if (y < 0 || y >= screen->height)
        y = y % screen->height;

    // get center of text
    int string_len = strlen(text);
    int text_len = 0;
    for (int i = 0; i < string_len; i++)
    {
        text_len += char_width(text[i], fdes) + 1;
    }

    // draw text into the screen buffer
    for (int i = 0; i < string_len; i++)
    {
        draw_char(x - text_len / 2, y, text[i], fdes, color, screen);
        x += char_width(text[i], fdes) + 1;
    }
}

// - function ----------------------------------------------------------------
unsigned char *load_ppm_image(char *filename, int *width, int *height, int *color)
{

    // open file pointer
    FILE *file = fopen(filename, "rb");
    if (file == NULL)
    {
        fprintf(stderr, "Error: file[%s] does not exist.\n", filename);
        return NULL;
    }

    // read header
    if (fscanf(file, "P6 %d %d %d\n", width, height, color) != 3)
    {
        fprintf(stderr, "Error: invalid image format.\n");
        return NULL;
    }

    // allocate memory for image
    int size = (*width) * (*height) * 3;
    unsigned char *image = (unsigned char *)malloc(size);
    if (image == NULL)
    {
        fprintf(stderr, "Error: memory allocation failed.\n");
        return NULL;
    }

    // read image
    if (fread(image, sizeof(unsigned char), size, file) != size)
    {
        fprintf(stderr, "Error: image reading error.\n");
        return NULL;
    }

    fclose(file);
    return image;
}

// - function ----------------------------------------------------------------
void ppm_to_framebuffer(unsigned char *image, framebuffer_t *screen)
{
    // copy image to screen buffer
    for (int i = 0; i < screen->width * screen->height; i++)
    {
        color_t color = {image[3 * i + 0], image[3 * i + 1], image[3 * i + 2]};
        screen->pixels[i] = rgb_to_short(color);
    }
}

// - function ----------------------------------------------------------------
void invert_screen(double opacity, framebuffer_t *screen)
{
    if (opacity < 0)
        return;
        
    // invert frame buffer colors
    for (int u = 0; u < screen->width; u++)
    {
        for (int v = 0; v < screen->height; v++)
        {
            color_t color = short_to_rgb(screen->pixels[u + v * screen->width]);
            color.r = (int)(color.r * opacity);
            color.g = (int)(color.g * opacity);
            color.b = (int)(color.b * opacity);
            screen->pixels[u + v * screen->width] = rgb_to_short(color);
        }
    }
}