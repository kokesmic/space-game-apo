/*
 * File name: renderer.h
 * Date:      2022/05/05 16:22
 * Author:    Michal Kokeš
 * License:   MIT
 *
 * Description:
 * Contains rendering functions and color definitions.
 */

#ifndef __RENDERER_H__
#define __RENDERER_H__

#include "font_types.h"
#include <stdbool.h>

// -- DEFINE COLORS -----------------------------------------------------------
#define RGB_COLOR_WHITE             {255, 255, 255};
#define RGB_COLOR_BLACK                {20, 20, 20};
#define RGB_COLOR_DARK_GRAY            {20, 20, 20};
#define RGB_COLOR_GRAY              {100, 100, 100};
#define RGB_COLOR_LIGHT_GRAY        {100, 100, 100};
#define RGB_COLOR_RED               {255, 155, 155};
#define RGB_COLOR_BLUE              {155, 155, 255};
#define RGB_COLOR_GREEN             {155, 255, 155};

/**
 * @brief FrameBuffer has width and height and allocated pixels of size (width * height * 3)
 */
typedef struct FrameBuffer
{
    int width;
    int height;
    unsigned short *pixels;
} framebuffer_t;

/**
 * @brief Color contains three values (r, g ,b) to store color data
 */
typedef struct Color
{
    int r;
    int g;
    int b;
} color_t, rgb_t;

/**
 * @brief Draws circle with a specific color into the fram buffer.
 *
 * @param x X position on the screen.
 * @param y Y position on the screen.
 * @param radius Radius of the circle.
 * @param color Color of the circle.
 * @param loop If true, the coordinates are looped.
 * @param screen FrameBuffer to draw into.
 */
void draw_circle(int x, int y, int radius, color_t color, bool loop, framebuffer_t *screen);

/**
 * @brief Draws circle with a specific color into and different color for the outline into the fram buffer.
 *
 * @param x X position on the screen.
 * @param y Y position on the screen.
 * @param radius Radius of the circle.
 * @param color Color of the circle.
 * @param outline Color of the outline.
 * @param screen FrameBuffer to draw into.
 */
void draw_circle_outline(int x, int y, int radius, int border, color_t color, color_t outline, framebuffer_t *screen);

/**
 * @brief Draws rectangle with a specific color into the fram buffer.
 *
 * @param x X position on the screen.
 * @param y Y position on the screen.
 * @param width Width of the rectangle
 * @param height Height of the rectangle
 * @param color Color of the rectangle.
 * @param screen FrameBuffer to draw into.
 */
void draw_rect(int x, int y, int width, int height, color_t color, framebuffer_t *screen);

/**
 * @brief Draws text with a specific color into the framebuffer.
 *
 * @param x X position on the screen.
 * @param y Y position on the screen.
 * @param text Text to draw.
 * @param color  Color of the text.
 * @param font_des Font descriptor.
 * @param screen FrameBuffer to draw into.
 */
void draw_text(int x, int y, char *text, color_t color, font_descriptor_t *fdes, framebuffer_t *screen);

/**
 * @brief Draws text with a specific color into the framebuffer, centered around x coordinate.
 *
 * @param x X position on the screen.
 * @param y Y position on the screen.
 * @param text Text to draw.
 * @param color  Color of the text.
 * @param font_des Font descriptor.
 * @param screen FrameBuffer to draw into.
 */
void draw_text_centered(int x, int y, char *text, color_t color, font_descriptor_t *fdes, framebuffer_t *screen);

/**
 * @brief Copies a framebuffer into another.
 *
 * @param src Source framebuffer.
 * @param dest Destination framebuffer.
 */
void copy_frame_buffer(framebuffer_t *src, framebuffer_t *dest);

/**
 * @brief Draws a pixel with a specific color into the framebuffer.
 *
 * @param x X position on the screen.
 * @param y Y position on the screen.
 * @param color Color of the pixel.
 * @param loop bool when the pixel is outside the framebuffer it will be drawn in the other side.
 * @param screen FrameBuffer to draw into.
 */
void draw_pixel(int x, int y, color_t color, bool loop, framebuffer_t *screen);

/**
 * @brief Translates Color to short.
 *
 * @param color rgb Color to translate.
 * @return unsigned short Translated color.
 */
unsigned short rgb_to_short(color_t color);

/**
 * @brief Translates short to Color.
 *
 * @param color short Color to translate.
 * @return color_t Translated color.
 */
color_t short_to_rgb(unsigned short color);

/**
 * @brief Draw framebuffer to the screen.
 *
 * @param screen FrameBuffer to draw.
 * @param mem_base Base address of the framebuffer.
 */
void draw_screen(framebuffer_t *screen, unsigned char *mem_base);

/**
 * @brief Clears the framebuffer.
 *
 * @param screen FrameBuffer to clear.
 */
void clear_screen(framebuffer_t *screen);

/**
 * @brief Draws a character into the screen buffer.
 *
 * @param x x coordinate of the character.
 * @param y y coordinate of the character.
 * @param ch  character to draw.
 * @param fdes font descriptor.
 * @param color color of the character.
 * @param screen framebuffer to draw into.
 */
void draw_char(int x, int y, char ch, font_descriptor_t *fdes, color_t color, framebuffer_t *screen);

/**
 * @brief moves r colors to the left and g colors to the right
 *
 * @param double opacity of the effect.
 * @param framebuffer_t *screen
 */
void invert_screen(double opacity, framebuffer_t *screen);

/**
 * @brief Loads image from file.
 *
 * @param filename Name of the file to load.
 * @param width Width of the image loaded.
 * @param height Height of the image loaded.
 * @return unsigned char* Pointer to the image or NULL if failed.
 */
unsigned char *load_ppm_image(char *filename, int *width, int *height, int *color);

/**
 * @brief Saves image to framebuffer.
 *
 * @param data Data of the image.
 * @param screen FrameBuffer to save the image to.
 */
void ppm_to_framebuffer(unsigned char *data, framebuffer_t *screen);

#endif
/* end of renderer.h */