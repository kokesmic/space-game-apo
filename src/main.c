/*
 * File name: main.c
 * Date:      2022/05/19 02:59
 * Author:    Michal Kokeš and Pavel Klas
 * License:   MIT
 *
 * Description:
 * Initializes and maps memory of prepherals on the board.
 * Draws main menu and lets player choose game mode or exit.
 * Then starts game.
 * When one player reaches 0 hp the death screen is displayed.
 * After that the player can choose to play again or exit.
 */

// - Defines ------------------------------------------------------------------
#define _POSIX_C_SOURCE 200112L
#define SCREEN_WIDTH 480
#define SCREEN_HEIGHT 320

// - Includes -----------------------------------------------------------------
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>
#include <termios.h>
#include <stdbool.h>

// - Game Includes -------------------------------------------------------------
#include "input.h"
#include "renderer.h"
#include "gameloop_screen.h"
#include "main_menu_screen.h"
#include "death_screen.h"

// - Board Includes ------------------------------------------------------------
#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "font_types.h"

// - Main function ------------------------------------------------------------
int main(int argc, char **argv)
{
  // - INITIALIZE GAME ---------------------------------------------------------
  printf("Starting game\n");

  // init random
  init_random();

  // init rendering
  printf("Initializing frame buffer...\n");
  framebuffer_t fb = {
      .width = SCREEN_WIDTH,
      .height = SCREEN_HEIGHT,
      .pixels = (unsigned short *)malloc(SCREEN_HEIGHT * SCREEN_WIDTH * 2)};

  // if pixel buffer failed to allocate then exit
  if (fb.pixels == NULL)
  {
    fprintf(stderr, "Error allocating frame buffer\n");
    return 1;
  }

  printf("Mapping board memory...\n");
  unsigned char *mem_base = map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);
  if (mem_base == NULL)
  {
    fprintf(stderr, "Error mapping physical address\n");
    return 2;
  }

  // init board
  printf("Mapping board lcd memory...\n");
  unsigned char *parlcd_mem_base = map_phys_address(PARLCD_REG_BASE_PHYS, PARLCD_REG_SIZE, 0);
  if (parlcd_mem_base == NULL)
  {
    fprintf(stderr, "Error mapping physical address\n");
    return 2;
  }

  // init lcd
  parlcd_hx8357_init(parlcd_mem_base);

  // setup refresh delay
  struct timespec loop_delay;
  loop_delay.tv_sec = 0;
  loop_delay.tv_nsec = 75 * 1000 * 1000;


  while (true)
  {
    // -- MAIN MENU --------------------------------------------------------------
    printf("Starting main menu...\n");
    int menu_selection;
    main_menu(&menu_selection, loop_delay, parlcd_mem_base, mem_base, &fb);
    printf("Menu selection: %d\n", menu_selection);
    
    // if player selected exit then exit
    if (menu_selection == EXIT)
      break;

    // -- GAME LOOP --------------------------------------------------------------
    printf("Starting game loop...\n");
    int winner;
    game_loop(&winner, menu_selection == COOP, parlcd_mem_base, mem_base, loop_delay, &fb, SCREEN_WIDTH, SCREEN_HEIGHT);

    // -- GAME OVER --------------------------------------------------------------
    printf("Game over\n");
    game_over(winner, parlcd_mem_base, mem_base, &fb, SCREEN_WIDTH, SCREEN_HEIGHT);
  }

  // -- EXIT --------------------------------------------------------------
  printf("Clearing memory...\n");

  // draw black screen
  clear_screen(&fb);
  draw_screen(&fb, parlcd_mem_base);

  // free memory
  free(fb.pixels);

  // exit
  printf("Exiting...\n");
  return 0;
}
