/*
 * File name: gameloop_screen.h
 * Date:      2022/05/18 22:17
 * Author:    Michal Kokeš
 * License:   MIT
 *
 * Description:
 * Contains game drawing method and game update method all neatly packed into game loop with a timer.
 */

// - defines -------------------------------------------------------------------
#define TEXTURE_BACKGROUND "/tmp/space-game/textures/background.ppm"
#define MAX_BULLETS 100

// - includes ------------------------------------------------------------------
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <termios.h>
#include <stdbool.h>
#include <stdint.h>
#include <math.h>
#include "gameloop_screen.h"
#include "vector.h"
#include "player.h"
#include "input.h"
#include "projectile.h"

// - function ------------------------------------------------------------------
void game_loop(int *winner, bool coop, unsigned char *lcd_mem_base, unsigned char *mem_base, struct timespec loop_delay, framebuffer_t *fb, int screen_width, int screen_height)
{
    // load image and convert to framebuffer
    int w, h, c;
    unsigned char *img = load_ppm_image(TEXTURE_BACKGROUND, &w, &h, &c);
    framebuffer_t background = {.width = w, .height = h, .pixels = (unsigned short *)malloc(h * w * 2)};
    ppm_to_framebuffer(img, &background);
    free(img);

    // init player 1
    color_t player_1_color = {255, 155, 155};                  // red color
    vector2_t player_1_pos = {fb->width / 2, fb->height - 20}; // bottom center
    vector2_t player_1_vel = {0, 0};                           // no velocity
    player_t player_1;
    player_init(&player_1, 1, false, player_1_color, player_1_pos, player_1_vel);
    int player_1_bullet_count = 0;
    bullet_t player_1_bullets[MAX_BULLETS];

    // init player 2
    color_t player_2_color = {155, 155, 255};     // blue color
    vector2_t player_2_pos = {fb->width / 2, 20}; // top center
    vector2_t player_2_vel = {0, 0};              // no velocity
    player_t player_2;
    player_init(&player_2, 2, !coop, player_2_color, player_2_pos, player_2_vel);
    int player_2_bullet_count = 0;
    bullet_t player_2_bullets[MAX_BULLETS];

    // init both player directions
    vector2_t player_1_dir, player_2_dir;

    // init bullets
    for (int i = 0; i < MAX_BULLET_COUNT; i++)
    {
        player_1_bullets[i].active = false;
        player_1_bullets[i].position = (vector2_t){0, 0};
        player_1_bullets[i].velocity = (vector2_t){0, 0};
        player_1_bullets[i].lifetime = BULLET_LIFE_TIME;

        player_2_bullets[i].active = false;
        player_2_bullets[i].position = (vector2_t){0, 0};
        player_2_bullets[i].velocity = (vector2_t){0, 0};
        player_2_bullets[i].lifetime = BULLET_LIFE_TIME;
    }

    // init colors
    color_t outline = RGB_COLOR_WHITE;
    color_t black = RGB_COLOR_BLACK;
    color_t red = RGB_COLOR_RED;
    color_t dark_red = {100, 0, 0};
    color_t blue = RGB_COLOR_BLUE;
    color_t dark_blue = {0, 0, 100};

    // update and draw players and bullets every frame until player health is 0
    while (player_1.health > 0 && player_2.health > 0)
    {
        // UPDATE GAME

        // update leds
        set_led_value(0, mem_base, black);
        set_led_value(1, mem_base, black);

        // clear screen by memcopy image from background
        copy_frame_buffer(&background, fb);

        // get direction
        vector2_from_rotation(&player_1_dir, get_knob_rotation_value(2, mem_base));
        vector2_from_rotation(&player_2_dir, get_knob_rotation_value(0, mem_base));

        // get input
        bool player_1_shoot = false, player_2_shoot = false;
        bool player_1_button = get_knob_button_value(0, mem_base);
        bool player_2_button = get_knob_button_value(2, mem_base);

        // if not coop get ai input and override input
        if (!coop)
        {
            get_ai_input(&player_2, &player_1, &player_2_dir, &player_2_shoot);
            player_2_button = true;
        }

        // update players
        player_update(&player_1, &player_1_shoot, player_1_button, player_1_dir, screen_width, screen_height);
        player_update(&player_2, &player_2_shoot, player_2_button, player_2_dir, screen_width, screen_height);

        // if player shoots add a new bullet
        if (player_1_shoot)
        {
            player_1_bullets[player_1_bullet_count].active = true;
            player_1_bullets[player_1_bullet_count].position = player_1.position;
            vector2_t bullet_dir = player_1_dir;
            vector2_multiply(&bullet_dir, 5);
            player_1_bullets[player_1_bullet_count].velocity = bullet_dir;
            player_1_bullets[player_1_bullet_count].lifetime = BULLET_LIFE_TIME;
            player_1_bullet_count++;
        }
        if (player_2_shoot)
        {
            player_2_bullets[player_2_bullet_count].active = true;
            player_2_bullets[player_2_bullet_count].position = player_2.position;
            vector2_t bullet_dir = player_2_dir;
            vector2_multiply(&bullet_dir, 5);
            player_2_bullets[player_2_bullet_count].velocity = bullet_dir;
            player_2_bullets[player_2_bullet_count].lifetime = BULLET_LIFE_TIME;
            player_2_bullet_count++;
        }

        // draw bullets at current position as a motion blur
        for (int i = 0; i < player_1_bullet_count; i++)
            draw_circle(player_1_bullets[i].position.x, player_1_bullets[i].position.y, 5, dark_red, false, fb);
        for (int i = 0; i < player_2_bullet_count; i++)
            draw_circle(player_2_bullets[i].position.x, player_2_bullets[i].position.y, 5, dark_blue, false, fb);

        // move and draw bullets for player 1
        bullet_update(player_1_bullet_count, player_1_bullets, screen_width, screen_height);
        for (int i = 0; i < player_1_bullet_count; i++)
        {
            // check collision with player and light up leds if collision
            bool blink_leds = false;
            player_bullet_collision(&player_2, &player_1_bullets[i], &blink_leds);
            bullet_reorder(&player_1_bullet_count, player_1_bullets);
            draw_circle(player_1_bullets[i].position.x, player_1_bullets[i].position.y, 5, player_1_color, false, fb);
            if (blink_leds)
                set_led_value(0, mem_base, blue);
        }

        // move and draw bullets for player 2
        bullet_update(player_2_bullet_count, player_2_bullets, screen_width, screen_height);
        for (int i = 0; i < player_2_bullet_count; i++)
        {
            // check collision with player and light up leds if collision
            bool blink_leds = false;
            player_bullet_collision(&player_1, &player_2_bullets[i], &blink_leds);
            bullet_reorder(&player_2_bullet_count, player_2_bullets);
            draw_circle(player_2_bullets[i].position.x, player_2_bullets[i].position.y, 5, player_2_color, false, fb);
            if (blink_leds)
                set_led_value(1, mem_base, red);
        }

        // RENDER PLAYERS
        // draw players as circle with outline
        draw_circle_outline(player_1.position.x, player_1.position.y, 9, 2, player_1.color, outline, fb);
        draw_circle_outline(player_2.position.x, player_2.position.y, 9, 2, player_2.color, outline, fb);

        // render player one
        vector2_t vel_normal_player_1 = player_1.velocity;
        vector2_multiply(&vel_normal_player_1, 5);
        draw_circle(player_1.position.x + vel_normal_player_1.x, player_1.position.y + vel_normal_player_1.y, 4, outline, true, fb);
        draw_circle(player_1.position.x + player_1_dir.x * 25, player_1.position.y + player_1_dir.y * 25, 3, player_1_color, true, fb);

        // render player two
        vector2_t vel_normal_player_2 = player_2.velocity;
        vector2_multiply(&vel_normal_player_2, 5);
        draw_circle(player_2.position.x + vel_normal_player_2.x, player_2.position.y + vel_normal_player_2.y, 4, outline, true, fb);
        draw_circle(player_2.position.x + player_2_dir.x * 25, player_2.position.y + player_2_dir.y * 25, 3, player_2_color, true, fb);

        // draw screen
        draw_screen(fb, lcd_mem_base);

        // draw health bars and cooldowns
        // clear leds
        uint8_t led_data_clear[4] = {0, 0, 0, 0};
        set_ledstrip_value(led_data_clear, mem_base);
        uint8_t led_data[4] = {player_scale_health(player_1.health, true), player_scale_health(player_1.cooldown * 5, true), player_scale_health(player_2.cooldown * 5, false), player_scale_health(player_2.health, false)};
        set_ledstrip_value(led_data, mem_base);

        clock_nanosleep(CLOCK_MONOTONIC, 0, &loop_delay, NULL);
    }
    *winner = player_1.health > 0 ? 1 : 2;
    free(background.pixels);
}
