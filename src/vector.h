/*
 * File name: vector.c
 * Date:      2022/05/08 12:41
 * Author:    Michal Kokeš
 * License:   MIT
 * 
 * Description:
 * Vector struct containing vector data and functions for manipulating it.
 */

#ifndef __VECTOR_H__
#define __VECTOR_H__

/**
 * @brief Vector2 represents a point or a vector in a 2D plane.
 */
typedef struct Vector2
{
    double x;
    double y;
} vector2_t;

/**
 * @brief Adds 2 vectors together.
 *
 * @param v1 first vector to added and returned.
 * @param v2  second vector to add.
 * @return vector2_t* (v1.x + v2.x , v1.y + v2.y).
 */
vector2_t *vector2_add(vector2_t *v1, vector2_t v2);

/**
 * @brief Subtracts 2 vectors.
 * 
 * @param v1 first vector to added and returned.
 * @param v2 second vector to add.
 * @return vector2_t* vector2_t (v1.x - v2.x , v1.y - v2.y).
 */
vector2_t *vector2_sub(vector2_t *v1, vector2_t v2);

/**
 * @brief Multiplies both values in the vector by n.
 *
 * @param v1 vector to be multiplied and returned.
 * @param n number to multiply the vector with.
 * @return vector2_t (v1.x * n, v1.y * n).
 */
vector2_t *vector2_multiply(vector2_t *v1, double n);

/**
 * @brief Calculates the distance between two vectors.
 *
 * @param v1 first vector to to calculate distance from.
 * @param v2 second vector to calculate distance between.
 * @return float distance between the the vector.
 */
float vector2_distance(vector2_t v1, vector2_t v2);

/**
 * @brief Claculates directional vector from a rotation.
 *
 * @param v1 vector to store calculated direction into.
 * @param rotation rotation to calculate direction from.
 */
void vector2_from_rotation(vector2_t *v1, double rotation);

/**
 * @brief Clamps the vector to a maximum value.
 *
 * @param v1 vector to clamp.
 * @param min minimum value to clamp to.
 * @param max maximum value to clamp to.
 */
void vector2_clamp(vector2_t *v1, double min, double max);

/**
 * @brief Modulo of the vector. Warning: if val < 0 then val + modulo.
 *
 * @param v1 vector to calculate modulo of itself into.
 * @param mod_x number to calculate modulo of x with.
 * @param mod_y number to calculate modulo of y with.
 */
void vector2_modulo(vector2_t *v1, double mod_x, double mod_y);

/**
 * @brief Normalizes the vector.
 * 
 * @param v1 vector to normalize.
 */
void vector2_normalize(vector2_t *v1);

/**
 * @brief Calculates Normal vector of the vector.
 * 
 * @param v1 vector to calculate normal vector of.
 * @return vector2_t normal vector of the vector.
 */
 void vector2_normal(vector2_t *v1, vector2_t v2);
#endif
/* end of vector.h */