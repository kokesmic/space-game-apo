# Space game Apo

A 2 player simple 2d space shooter game that will be run and controlled by MicroZed APO board.
Also features a single player mode vs AI.


# HOW TO PLAY:
main menu:
	
    pressing red knob → single player against AI
	
    pressing green knob → two players against each other
	
    pressing blue knob → exit application

game:

	pressing red knob → red player shoots

	holdnig red knob → red player moves in knob direction

	direction of red knob → direction of red player

	pressing blue knob → blue player shoots

	holding blue knob → blue player moves in knob direction

	direction of blue knob → direction of blue player

end menu:
	
    pressing any of the knobs → return to main menu



# How to compile and run:
1. Make sure u have board MZ_APO with the module micro_zed
2. Clone this git repository
3. Replace the ip address on line 18 inside the "src/Makefile"
5. Now u can run "make run" inside src
6. ssh into the board with root password
7. everyhing should be loaded now :)
8. sometimes makefile corrupts the depends folder if that happenes just delete the depends folder and go to step 5




# Gameplay plan

Look at images below for more clear image of how this project will look.

Menu screen -

    Select between playing against AI, another player or quit the game in this menu.

Game loop -

    Control each of the two ships with a knob placed on the board
    
    You will also be able to shoot with each ship by pressing the respective knob.

    If a player touches a bullet that is not his color he will take damage and his hp will be reduced.
    
    After player takes 10 hits he loses and the game transitions to the defeat screen.

Defeat screen -

    Displays which player lost and prints random victory/defeat messages to each player.
    
    When any of the knobs is pressed, redirects the player back to the main menu.


# Work distribution
Michal Kokeš:

    - rendering
    - gameloop
    - collision

Pavel Klas:

    - screen: Main menu
    - screen: Defeat screen
    - input interface

# Used technologies

    - Github
    - MicroZed APO board.

# Controlls
main menu screen
![main menu screen](images/game_menu_screen.png)
gameloop screen
![main menu screen](images/gameloop_screen.png)
defeat screen
![main menu screen](images/victory_screen.png)
UML diagram
![](images/UML_diagram.png)
